export interface Vote {
    deviceId: string;
    language: string;
    time: number;
    fullName: string;
    group: string;
    extras: {
        [prop: string]: any;
    };
}
