import { Controller, Get, Param, Post, Delete, Put, Body } from '@nestjs/common';
import { VotesService } from './services/votes.service';
import { Vote } from './models/vote';

@Controller('api/votes')
export class VotesController {
  constructor(private readonly votes: VotesService) { }

  @Get(':deviceId')
  getVoteById(@Param('deviceId') deviceId: string): Vote | string {
    return this.votes.getVote(deviceId);
  }

  @Get()
  getVotes(): Vote[] {
    return [...this.votes.getAllVotes()];
  }

  @Post()
  vote(@Body() vote: Vote) {
    return this.votes.addVote(vote);
  }

  @Put()
  updateVote(@Body() vote: Vote): Vote {
    return this.votes.updateVote(vote);
  }

  @Delete(':deviceId')
  deleteVoteById(@Param('deviceId') deviceId: string): Vote {
    return this.votes.deleteVote(deviceId);
  }

  @Delete()
  clearVotes() {
    return this.votes.clearVotes();
  }



}
