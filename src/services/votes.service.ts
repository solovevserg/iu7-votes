import { Injectable } from '@nestjs/common';
import { Vote } from '../models/vote';

@Injectable()
export class VotesService {

  private votes = new Map<string, Vote>();

  addVote(vote: Vote) {
    if (!vote.deviceId || typeof vote.deviceId !== 'string') {
      throw Error('Incorrect vote.');
    }
    if (this.votes.has(vote.deviceId)) {
      throw Error('Vote already exists');
    }

    this.votes.set(vote.deviceId, vote);
    return vote;
  }

  getVote(deviceId: string) {
    const vote = this.votes.get(deviceId) || 'null';
    // if (!vote) {
    //   throw Error('There is no such vote.');
    // }
    return vote;
  }

  getAllVotes() {
    return this.votes.values();
  }

  deleteVote(deviceId: string) {
    if (!this.votes.has(deviceId)) {
      throw Error('There is no such vote.');
    }
    const vote = this.votes.get(deviceId);
    this.votes.delete(deviceId);
    return vote;
  }

  updateVote(vote: Vote) {
    if (!this.votes.has(vote.deviceId)) {
      throw Error('There is no such vote.');
    }
    this.votes.set(vote.deviceId, vote);
    return vote;
  }

  clearVotes() {
    const size = this.votes.size;
    this.votes.clear();
    return size;
  }
}
